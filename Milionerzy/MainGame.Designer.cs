﻿namespace Milionerzy
{
    partial class MainGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainGame));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.MainQuest = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.Question = new System.Windows.Forms.Label();
            this.LeftTopAnswerLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.AAnswer = new System.Windows.Forms.Label();
            this.ALabel = new System.Windows.Forms.Label();
            this.RightTopAnswerLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.BLabel = new System.Windows.Forms.Label();
            this.BAnswer = new System.Windows.Forms.Label();
            this.LeftBottomAnswerLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.CLabel = new System.Windows.Forms.Label();
            this.CAnswer = new System.Windows.Forms.Label();
            this.RightBottomAnswerLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.DLabel = new System.Windows.Forms.Label();
            this.DAnswer = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.helpPublic = new System.Windows.Forms.TableLayoutPanel();
            this.helpPhone = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.check6 = new System.Windows.Forms.TableLayoutPanel();
            this.check2 = new System.Windows.Forms.TableLayoutPanel();
            this.check4 = new System.Windows.Forms.TableLayoutPanel();
            this.check3 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.check1 = new System.Windows.Forms.TableLayoutPanel();
            this.check5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.help50 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.MainQuest.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.LeftTopAnswerLayout.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.RightTopAnswerLayout.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.LeftBottomAnswerLayout.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.RightBottomAnswerLayout.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(621, 310);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel2.BackgroundImage")));
            this.tableLayoutPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.MainQuest, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.LeftTopAnswerLayout, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.RightTopAnswerLayout, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.LeftBottomAnswerLayout, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.RightBottomAnswerLayout, 1, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(434, 310);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // MainQuest
            // 
            this.MainQuest.BackColor = System.Drawing.Color.Transparent;
            this.MainQuest.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MainQuest.BackgroundImage")));
            this.MainQuest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MainQuest.ColumnCount = 3;
            this.tableLayoutPanel2.SetColumnSpan(this.MainQuest, 2);
            this.MainQuest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.MainQuest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.MainQuest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.MainQuest.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.MainQuest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainQuest.Location = new System.Drawing.Point(3, 189);
            this.MainQuest.Name = "MainQuest";
            this.MainQuest.RowCount = 1;
            this.MainQuest.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainQuest.Size = new System.Drawing.Size(428, 40);
            this.MainQuest.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.Question, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(67, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 76F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(293, 34);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // Question
            // 
            this.Question.AutoSize = true;
            this.Question.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Question.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Question.ForeColor = System.Drawing.SystemColors.Control;
            this.Question.Location = new System.Drawing.Point(3, 4);
            this.Question.Name = "Question";
            this.Question.Size = new System.Drawing.Size(287, 25);
            this.Question.TabIndex = 0;
            this.Question.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LeftTopAnswerLayout
            // 
            this.LeftTopAnswerLayout.BackColor = System.Drawing.Color.Transparent;
            this.LeftTopAnswerLayout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LeftTopAnswerLayout.BackgroundImage")));
            this.LeftTopAnswerLayout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LeftTopAnswerLayout.ColumnCount = 1;
            this.LeftTopAnswerLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LeftTopAnswerLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LeftTopAnswerLayout.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.LeftTopAnswerLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftTopAnswerLayout.Location = new System.Drawing.Point(0, 235);
            this.LeftTopAnswerLayout.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.LeftTopAnswerLayout.Name = "LeftTopAnswerLayout";
            this.LeftTopAnswerLayout.RowCount = 1;
            this.LeftTopAnswerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LeftTopAnswerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.LeftTopAnswerLayout.Size = new System.Drawing.Size(217, 25);
            this.LeftTopAnswerLayout.TabIndex = 2;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.73783F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.56305F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.69913F));
            this.tableLayoutPanel5.Controls.Add(this.AAnswer, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.ALabel, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(217, 25);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // AAnswer
            // 
            this.AAnswer.AutoSize = true;
            this.AAnswer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AAnswer.ForeColor = System.Drawing.SystemColors.Control;
            this.AAnswer.Location = new System.Drawing.Point(77, 0);
            this.AAnswer.Name = "AAnswer";
            this.AAnswer.Size = new System.Drawing.Size(120, 25);
            this.AAnswer.TabIndex = 0;
            this.AAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ALabel
            // 
            this.ALabel.AutoSize = true;
            this.ALabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ALabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ALabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ALabel.Location = new System.Drawing.Point(39, 0);
            this.ALabel.Margin = new System.Windows.Forms.Padding(0);
            this.ALabel.Name = "ALabel";
            this.ALabel.Size = new System.Drawing.Size(35, 25);
            this.ALabel.TabIndex = 1;
            this.ALabel.Text = "A:";
            this.ALabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RightTopAnswerLayout
            // 
            this.RightTopAnswerLayout.BackColor = System.Drawing.Color.Transparent;
            this.RightTopAnswerLayout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RightTopAnswerLayout.BackgroundImage")));
            this.RightTopAnswerLayout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RightTopAnswerLayout.ColumnCount = 1;
            this.RightTopAnswerLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.RightTopAnswerLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.RightTopAnswerLayout.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.RightTopAnswerLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RightTopAnswerLayout.Location = new System.Drawing.Point(217, 235);
            this.RightTopAnswerLayout.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.RightTopAnswerLayout.Name = "RightTopAnswerLayout";
            this.RightTopAnswerLayout.RowCount = 1;
            this.RightTopAnswerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.RightTopAnswerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.RightTopAnswerLayout.Size = new System.Drawing.Size(217, 25);
            this.RightTopAnswerLayout.TabIndex = 3;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.865169F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.78651F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.34831F));
            this.tableLayoutPanel7.Controls.Add(this.BLabel, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.BAnswer, 2, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(217, 25);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // BLabel
            // 
            this.BLabel.AutoSize = true;
            this.BLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.BLabel.Location = new System.Drawing.Point(14, 0);
            this.BLabel.Margin = new System.Windows.Forms.Padding(0);
            this.BLabel.Name = "BLabel";
            this.BLabel.Size = new System.Drawing.Size(35, 25);
            this.BLabel.TabIndex = 0;
            this.BLabel.Text = "B:";
            this.BLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BAnswer
            // 
            this.BAnswer.AutoSize = true;
            this.BAnswer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BAnswer.ForeColor = System.Drawing.SystemColors.Control;
            this.BAnswer.Location = new System.Drawing.Point(49, 0);
            this.BAnswer.Margin = new System.Windows.Forms.Padding(0);
            this.BAnswer.Name = "BAnswer";
            this.BAnswer.Size = new System.Drawing.Size(128, 25);
            this.BAnswer.TabIndex = 1;
            this.BAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LeftBottomAnswerLayout
            // 
            this.LeftBottomAnswerLayout.BackColor = System.Drawing.Color.Transparent;
            this.LeftBottomAnswerLayout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LeftBottomAnswerLayout.BackgroundImage")));
            this.LeftBottomAnswerLayout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LeftBottomAnswerLayout.ColumnCount = 1;
            this.LeftBottomAnswerLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LeftBottomAnswerLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LeftBottomAnswerLayout.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.LeftBottomAnswerLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftBottomAnswerLayout.Location = new System.Drawing.Point(0, 266);
            this.LeftBottomAnswerLayout.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.LeftBottomAnswerLayout.Name = "LeftBottomAnswerLayout";
            this.LeftBottomAnswerLayout.RowCount = 1;
            this.LeftBottomAnswerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LeftBottomAnswerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.LeftBottomAnswerLayout.Size = new System.Drawing.Size(217, 25);
            this.LeftBottomAnswerLayout.TabIndex = 4;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.73913F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.56522F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.695652F));
            this.tableLayoutPanel6.Controls.Add(this.CLabel, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.CAnswer, 2, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(217, 25);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // CLabel
            // 
            this.CLabel.AutoSize = true;
            this.CLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.CLabel.Location = new System.Drawing.Point(39, 0);
            this.CLabel.Margin = new System.Windows.Forms.Padding(0);
            this.CLabel.Name = "CLabel";
            this.CLabel.Size = new System.Drawing.Size(35, 25);
            this.CLabel.TabIndex = 0;
            this.CLabel.Text = "C:";
            this.CLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CAnswer
            // 
            this.CAnswer.AutoSize = true;
            this.CAnswer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CAnswer.ForeColor = System.Drawing.SystemColors.Control;
            this.CAnswer.Location = new System.Drawing.Point(77, 0);
            this.CAnswer.Name = "CAnswer";
            this.CAnswer.Size = new System.Drawing.Size(120, 25);
            this.CAnswer.TabIndex = 1;
            this.CAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RightBottomAnswerLayout
            // 
            this.RightBottomAnswerLayout.BackColor = System.Drawing.Color.Transparent;
            this.RightBottomAnswerLayout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RightBottomAnswerLayout.BackgroundImage")));
            this.RightBottomAnswerLayout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RightBottomAnswerLayout.ColumnCount = 1;
            this.RightBottomAnswerLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.RightBottomAnswerLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.RightBottomAnswerLayout.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.RightBottomAnswerLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RightBottomAnswerLayout.Location = new System.Drawing.Point(217, 266);
            this.RightBottomAnswerLayout.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.RightBottomAnswerLayout.Name = "RightBottomAnswerLayout";
            this.RightBottomAnswerLayout.RowCount = 1;
            this.RightBottomAnswerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.RightBottomAnswerLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.RightBottomAnswerLayout.Size = new System.Drawing.Size(217, 25);
            this.RightBottomAnswerLayout.TabIndex = 5;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.865169F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.78651F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.34831F));
            this.tableLayoutPanel8.Controls.Add(this.DLabel, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.DAnswer, 2, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(217, 25);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // DLabel
            // 
            this.DLabel.AutoSize = true;
            this.DLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.DLabel.Location = new System.Drawing.Point(14, 0);
            this.DLabel.Margin = new System.Windows.Forms.Padding(0);
            this.DLabel.Name = "DLabel";
            this.DLabel.Size = new System.Drawing.Size(35, 25);
            this.DLabel.TabIndex = 0;
            this.DLabel.Text = "D:";
            this.DLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DAnswer
            // 
            this.DAnswer.AutoSize = true;
            this.DAnswer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DAnswer.ForeColor = System.Drawing.SystemColors.Control;
            this.DAnswer.Location = new System.Drawing.Point(49, 0);
            this.DAnswer.Margin = new System.Windows.Forms.Padding(0);
            this.DAnswer.Name = "DAnswer";
            this.DAnswer.Size = new System.Drawing.Size(128, 25);
            this.DAnswer.TabIndex = 1;
            this.DAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel3.BackgroundImage")));
            this.tableLayoutPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel3.Controls.Add(this.helpPublic, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.helpPhone, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.help50, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(434, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(187, 310);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // helpPublic
            // 
            this.helpPublic.BackColor = System.Drawing.Color.Transparent;
            this.helpPublic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("helpPublic.BackgroundImage")));
            this.helpPublic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.helpPublic.ColumnCount = 1;
            this.helpPublic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.helpPublic.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.helpPublic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpPublic.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.helpPublic.Location = new System.Drawing.Point(129, 5);
            this.helpPublic.Margin = new System.Windows.Forms.Padding(5);
            this.helpPublic.Name = "helpPublic";
            this.helpPublic.RowCount = 1;
            this.helpPublic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.helpPublic.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.helpPublic.Size = new System.Drawing.Size(53, 52);
            this.helpPublic.TabIndex = 3;
            // 
            // helpPhone
            // 
            this.helpPhone.BackColor = System.Drawing.Color.Transparent;
            this.helpPhone.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("helpPhone.BackgroundImage")));
            this.helpPhone.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.helpPhone.ColumnCount = 1;
            this.helpPhone.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.helpPhone.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.helpPhone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpPhone.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.helpPhone.Location = new System.Drawing.Point(67, 5);
            this.helpPhone.Margin = new System.Windows.Forms.Padding(5);
            this.helpPhone.Name = "helpPhone";
            this.helpPhone.RowCount = 1;
            this.helpPhone.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.helpPhone.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.helpPhone.Size = new System.Drawing.Size(52, 52);
            this.helpPhone.TabIndex = 2;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel9.ColumnCount = 5;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel9, 3);
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel24, 4, 2);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel23, 3, 2);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel22, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel21, 4, 1);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel20, 3, 1);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel19, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel18, 4, 0);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel17, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel16, 4, 3);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel11, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.check6, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.check2, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.check4, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.check3, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label2, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.check1, 1, 5);
            this.tableLayoutPanel9.Controls.Add(this.check5, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel10, 2, 5);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel12, 2, 4);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel13, 3, 4);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel14, 2, 3);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel15, 3, 3);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(5, 62);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 7;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(182, 248);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel24.BackgroundImage")));
            this.tableLayoutPanel24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel24.ColumnCount = 1;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(144, 70);
            this.tableLayoutPanel24.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 1;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(38, 35);
            this.tableLayoutPanel24.TabIndex = 23;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel23.BackgroundImage")));
            this.tableLayoutPanel23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel23.ColumnCount = 1;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(108, 70);
            this.tableLayoutPanel23.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 1;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel23.TabIndex = 22;
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel22.BackgroundImage")));
            this.tableLayoutPanel22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel22.ColumnCount = 1;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(72, 70);
            this.tableLayoutPanel22.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 1;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel22.TabIndex = 21;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel21.BackgroundImage")));
            this.tableLayoutPanel21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel21.ColumnCount = 1;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(144, 35);
            this.tableLayoutPanel21.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(38, 35);
            this.tableLayoutPanel21.TabIndex = 20;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel20.BackgroundImage")));
            this.tableLayoutPanel20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel20.ColumnCount = 1;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(108, 35);
            this.tableLayoutPanel20.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel20.TabIndex = 19;
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel19.BackgroundImage")));
            this.tableLayoutPanel19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(72, 35);
            this.tableLayoutPanel19.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel19.TabIndex = 18;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel18.BackgroundImage")));
            this.tableLayoutPanel18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel18.ColumnCount = 1;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(144, 0);
            this.tableLayoutPanel18.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(38, 35);
            this.tableLayoutPanel18.TabIndex = 15;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel17.BackgroundImage")));
            this.tableLayoutPanel17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel17.ColumnCount = 1;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(108, 0);
            this.tableLayoutPanel17.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel17.TabIndex = 17;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel16.BackgroundImage")));
            this.tableLayoutPanel16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(144, 105);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(38, 35);
            this.tableLayoutPanel16.TabIndex = 15;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel11.BackgroundImage")));
            this.tableLayoutPanel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(72, 0);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel11.TabIndex = 12;
            // 
            // check6
            // 
            this.check6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.check6.ColumnCount = 1;
            this.check6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.check6.Location = new System.Drawing.Point(45, 0);
            this.check6.Margin = new System.Windows.Forms.Padding(0);
            this.check6.Name = "check6";
            this.check6.RowCount = 1;
            this.check6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check6.Size = new System.Drawing.Size(27, 35);
            this.check6.TabIndex = 10;
            // 
            // check2
            // 
            this.check2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.check2.ColumnCount = 1;
            this.check2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.check2.Location = new System.Drawing.Point(45, 140);
            this.check2.Margin = new System.Windows.Forms.Padding(0);
            this.check2.Name = "check2";
            this.check2.RowCount = 1;
            this.check2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check2.Size = new System.Drawing.Size(27, 35);
            this.check2.TabIndex = 9;
            this.check2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel14_Paint);
            // 
            // check4
            // 
            this.check4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.check4.ColumnCount = 1;
            this.check4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.check4.Location = new System.Drawing.Point(45, 70);
            this.check4.Margin = new System.Windows.Forms.Padding(0);
            this.check4.Name = "check4";
            this.check4.RowCount = 1;
            this.check4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check4.Size = new System.Drawing.Size(27, 35);
            this.check4.TabIndex = 9;
            // 
            // check3
            // 
            this.check3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.check3.ColumnCount = 1;
            this.check3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.check3.Location = new System.Drawing.Point(45, 105);
            this.check3.Margin = new System.Windows.Forms.Padding(0);
            this.check3.Name = "check3";
            this.check3.RowCount = 1;
            this.check3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check3.Size = new System.Drawing.Size(27, 35);
            this.check3.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 35);
            this.label7.TabIndex = 6;
            this.label7.Text = "6";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Location = new System.Drawing.Point(3, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 35);
            this.label2.TabIndex = 1;
            this.label2.Text = "1";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label3.Location = new System.Drawing.Point(3, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 35);
            this.label3.TabIndex = 2;
            this.label3.Text = "2";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label4.Location = new System.Drawing.Point(3, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 35);
            this.label4.TabIndex = 3;
            this.label4.Text = "3";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label5.Location = new System.Drawing.Point(3, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 35);
            this.label5.TabIndex = 4;
            this.label5.Text = "4";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Location = new System.Drawing.Point(3, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 35);
            this.label6.TabIndex = 5;
            this.label6.Text = "5";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // check1
            // 
            this.check1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("check1.BackgroundImage")));
            this.check1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.check1.ColumnCount = 1;
            this.check1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.check1.Location = new System.Drawing.Point(45, 175);
            this.check1.Margin = new System.Windows.Forms.Padding(0);
            this.check1.Name = "check1";
            this.check1.RowCount = 1;
            this.check1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check1.Size = new System.Drawing.Size(27, 35);
            this.check1.TabIndex = 7;
            // 
            // check5
            // 
            this.check5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.check5.ColumnCount = 1;
            this.check5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.check5.Location = new System.Drawing.Point(45, 35);
            this.check5.Margin = new System.Windows.Forms.Padding(0);
            this.check5.Name = "check5";
            this.check5.RowCount = 1;
            this.check5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.check5.Size = new System.Drawing.Size(27, 35);
            this.check5.TabIndex = 8;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel10.BackgroundImage")));
            this.tableLayoutPanel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(72, 175);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel10.TabIndex = 11;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel12.BackgroundImage")));
            this.tableLayoutPanel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(72, 140);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel12.TabIndex = 13;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel13.BackgroundImage")));
            this.tableLayoutPanel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(108, 140);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel13.TabIndex = 14;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel14.BackgroundImage")));
            this.tableLayoutPanel14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(72, 105);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel14.TabIndex = 15;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel15.BackgroundImage")));
            this.tableLayoutPanel15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(108, 105);
            this.tableLayoutPanel15.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(36, 35);
            this.tableLayoutPanel15.TabIndex = 16;
            // 
            // help50
            // 
            this.help50.BackColor = System.Drawing.Color.Transparent;
            this.help50.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("help50.BackgroundImage")));
            this.help50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.help50.ColumnCount = 1;
            this.help50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.help50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.help50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.help50.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.help50.Location = new System.Drawing.Point(5, 5);
            this.help50.Margin = new System.Windows.Forms.Padding(5);
            this.help50.Name = "help50";
            this.help50.RowCount = 1;
            this.help50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.help50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.help50.Size = new System.Drawing.Size(52, 52);
            this.help50.TabIndex = 1;
            // 
            // MainGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 310);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainGame";
            this.Text = "MainGame";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.MainQuest.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.LeftTopAnswerLayout.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.RightTopAnswerLayout.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.LeftBottomAnswerLayout.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.RightBottomAnswerLayout.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel MainQuest;
        private System.Windows.Forms.TableLayoutPanel RightTopAnswerLayout;
        private System.Windows.Forms.TableLayoutPanel LeftBottomAnswerLayout;
        private System.Windows.Forms.TableLayoutPanel RightBottomAnswerLayout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label Question;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label BLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label CLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label DLabel;
        private System.Windows.Forms.Label CAnswer;
        private System.Windows.Forms.TableLayoutPanel LeftTopAnswerLayout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label AAnswer;
        private System.Windows.Forms.Label ALabel;
        private System.Windows.Forms.Label BAnswer;
        private System.Windows.Forms.Label DAnswer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel check2;
        private System.Windows.Forms.TableLayoutPanel check4;
        private System.Windows.Forms.TableLayoutPanel check3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel check1;
        private System.Windows.Forms.TableLayoutPanel check5;
        private System.Windows.Forms.TableLayoutPanel check6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.TableLayoutPanel help50;
        private System.Windows.Forms.TableLayoutPanel helpPublic;
        private System.Windows.Forms.TableLayoutPanel helpPhone;
    }
}