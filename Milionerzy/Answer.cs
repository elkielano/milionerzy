﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milionerzy
{
    public class Answer
    {
        public string answer { get; set; }
        public bool isTrue { get; set; }    
        public char letter { get; set; }
    }
}
