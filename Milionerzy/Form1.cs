﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionerzy
{
    public partial class Form1 : Form
    {
        public event TickHandler Tick;
        public event TickHandler NewQuestion;
        public event TickHandler SelectAnswer;
        public event TickHandler Help;
        public event TickHandler Check;
        public event TickHandler RestartAnswerBackgrounds;
        public EventArgs e = null;
        public delegate void TickHandler(Form1 m, EventArgs e);
        MainGame targetForm;
        QuestionReader questionReader;
        private int fiftyCounter=0,phoneCounter=0,publicCounter=0;
        public Form1()
        {
            InitializeComponent();
            questionReader = new QuestionReader();
            //DataGridViewColumn column1 = dataGridView1.Columns[0];
            //  column1.Width = 300;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            var list = new BindingList<Question>(questionReader.questions);
            dataGridView1.DataSource = list;
            dataGridView1.Columns[1].Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pickQuestion.Enabled = true;
            button1.Enabled = false;
            // formState.Maximize(this);
            targetForm = new MainGame(this);
            //targetForm.LaunchOrigin = this;
           // targetForm.WindowState = FormWindowState.Maximized;
           // targetForm.FormBorderStyle = FormBorderStyle.None;
         //   targetForm.TopMost = true;
            targetForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //targetForm.WindowState = FormWindowState.Normal;
            //targetForm.FormBorderStyle = FormBorderStyle.FixedSingle;
            //targetForm.TopMost = false;
            //targetForm.Hide();
            Tick?.Invoke(this, e);

        }

        private void pickQuestion_Click(object sender, EventArgs e)
        {
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button2.Enabled = true;
            button7.Enabled = true;
            button8.Enabled = true;
            checkButton.Enabled = true;
            Question currentObject = (Question)dataGridView1.CurrentRow.DataBoundItem;
            QuestionReader.current = currentObject.Number;
            NewQuestion?.Invoke(this, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            QuestionReader.Selected = 'A';
            ///checkButton.Enabled = true;
            SelectAnswer?.Invoke(this, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            QuestionReader.Selected = 'B';
            ///checkButton.Enabled = true;
            SelectAnswer?.Invoke(this, e);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            QuestionReader.Selected = 'C';
            ///checkButton.Enabled = true;
            SelectAnswer?.Invoke(this, e);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            QuestionReader.Selected = 'D';
            ///checkButton.Enabled = true;
            SelectAnswer?.Invoke(this, e);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Jesteś pewien że chcesz wykorzystać połóweczkę?",
                                    "Potwierdź akcję!!",
                                    MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                fiftyCounter++;
                helpSwitch(1, fiftyCounter, button2);
                Help?.Invoke(this, e);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Jesteś pewien że chcesz wykorzystać telefon na melinę?",
                                    "Potwierdź akcję!!",
                                    MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                phoneCounter++;
                helpSwitch(2, phoneCounter, button7);
                Help?.Invoke(this, e);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void checkButton_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Jesteś pewien że chcesz sprawdzić pytanie ??",
                                     "Potwierdź akcję!!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                Check?.Invoke(this, e);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Jesteś pewien że chcesz wykorzystać ściepę publiki?",
                                    "Potwierdź akcję!!",
                                    MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                publicCounter++;
                helpSwitch(3, publicCounter, button8);
                Help?.Invoke(this, e);
            }
        }
        private void helpSwitch(int n,int counter,Button but)
        {
            HelpUtil.Instance.Number = n;
            but.Text = "Kliknij jeszcze raz by zakończyć pomoc";
            if (counter == 1)
            {
                HelpUtil.Instance.helpList[n-1].Selected = true;


            }
            else
            {
                HelpUtil.Instance.helpList[n-1].WasUsed = true;
                but.Enabled = false;
            }
            Help?.Invoke(this, e);
        }
    }
}
