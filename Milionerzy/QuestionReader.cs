﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionerzy
{
    public class QuestionReader
    {
        private string path = @"questions\questions.csv";
        public List<Question> questions { get; set; }
        public static int current{get;set;}
        public static char Selected { get; set; }
        public static int currentQuestion { get; set; }
        public QuestionReader()
        {
            questions= new List<Question>(); ;
            currentQuestion = 0;
            readQuestion();
        }
        public void readQuestion()
        {
            var reader = new StreamReader(path);
            Question quest = null;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (line.StartsWith("Quest"))
                {
                    quest = new Question();
                    string[] questList = line.Split(';');
                    quest.Number = Int32.Parse(questList[1]);
                    quest.Quest = questList[2];
                    quest.DedicatedTo = questList[3];
                }
                else if (line.StartsWith("Answer"))
                {
                    Answer ans = new Answer();
                    string[] ansList = line.Split(';');
                    char[] array = ansList[1].ToCharArray();
                    ans.letter = array[0];
                    ans.answer = ansList[2];
                    ans.isTrue = Boolean.Parse(ansList[3]);
                    quest.addAnswer(ans);
                }
                if (quest.Answers.Count == 4)
                {
                    questions.Add(quest);
                }
            }
        }
        public Question getQuestion()
        {
            foreach(Question q in questions)
            {
                if (q.Number == current)
                {
                    return q;
                }
            }
            return null;
        }
    }
}
