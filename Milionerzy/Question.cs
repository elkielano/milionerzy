﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milionerzy
{
   public class Question
    {
        public string Quest  { get; set; }
        public int Number { get; set; }
        public List<Answer> Answers { get; set; }
        public string DedicatedTo { get; set; }
        public Question()
        {
            Answers = new List<Answer>();
        }
        public void addAnswer(Answer answer)
        {
            Answers.Add(answer);
        }
        
    }
}
