﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milionerzy
{
    public class HelpUtil
    {
        public int Number{ get; set; }
        public List<Help> helpList;
        private static HelpUtil instance;
        private HelpUtil()
        {
            populateList();
        }
        public static HelpUtil Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new HelpUtil();
                }
                return instance;
            }
        }
        public void populateList()
        {
            Help h1 = new Help(1);
            Help h2 = new Help(1);
            Help h3 = new Help(1);
            helpList = new List<Help>();
            helpList.Add(h1);
            helpList.Add(h2);
            helpList.Add(h3);
        }
        public void restart()
        {
            populateList();
        }

    }
}
