﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milionerzy
{
    //1 for 50:50
    //2 for phone to friend
    //3 for audience voting
    public class Help
    {
        public int Number { get; set; }
        public bool WasUsed{ get; set; }
        public bool Selected { get; set; }


        public Help(int number)
        {
            Selected = false;
            WasUsed = false;
            Number = number;
        }
    }
}
