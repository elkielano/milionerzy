﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Milionerzy
{
    public partial class MainGame : Form
    {
        public Form1 form1;
        int count = 0;
        private QuestionReader questionReader;
        private char previousSelect = ' ';
        TableLayoutPanel[] labels;
        public MainGame(Form1 form)
        {
            questionReader = new QuestionReader();
            InitializeComponent();
            this.form1 = form;
            form1.Tick += new Form1.TickHandler(TestListener);
            form1.NewQuestion += new Form1.TickHandler(loadQuestion);
            form1.SelectAnswer += new Form1.TickHandler(selectAnswer);
            form1.Help += new Form1.TickHandler(help);
            form1.Check += new Form1.TickHandler(checkQuestion);
            labels = new TableLayoutPanel[]{check1,check2,check3,check4,check5,check6};
        }

        private void TestListener(Form1 f1,EventArgs e)
        {
            Image im = new Bitmap(@"graphics\leftBadAnswer.png");
            LeftTopAnswerLayout.BackgroundImage = im;

        }
        private void loadQuestion(Form f1, EventArgs e)
        {
            Question quest = questionReader.getQuestion();
            Question.Text = quest.Quest;
            AAnswer.Text = quest.Answers[0].answer;
            BAnswer.Text = quest.Answers[1].answer;
            CAnswer.Text = quest.Answers[2].answer;
            DAnswer.Text = quest.Answers[3].answer;
            restartAnswerBackgrounds();
            previousSelect = ' ';
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Image im = new Bitmap(@"graphics\leftSelectionAnswer.png");
            count++;
            if (count > 5)
            {
               im = new Bitmap(@"graphics\leftBadAnswer.png");
            }
            LeftTopAnswerLayout.BackgroundImage = im;

            // MainQuest.BackgroundImage = im;
        }
        private void checkQuestion(Form f1, EventArgs e)
        {
            Question quest = questionReader.getQuestion();
            foreach(Answer ans in quest.Answers){
                if (ans.isTrue)
                {
                    if (ans.letter == QuestionReader.Selected)
                    {
                        changeAnswerBackground(QuestionReader.Selected, "leftCorrectAnswer", "rightCorrectAnswer");
                        Image im = new Bitmap(@"graphics\whiteRectangle.png");
                        labels[QuestionReader.currentQuestion].BackgroundImage=im;
                        QuestionReader.currentQuestion++;
                        im = new Bitmap(@"graphics\orangeDiamond.png");
                        labels[QuestionReader.currentQuestion].BackgroundImage = im;

                    }
                    else
                    {
                        changeAnswerBackground(QuestionReader.Selected, "leftBadAnswer", "rightBadAnswer");
                        changeAnswerBackground(ans.letter, "leftCorrectAnswer", "rightCorrectAnswer");
                    }
                }
            }
        }
        private void selectAnswer(Form f1, EventArgs e)
        {
            if (QuestionReader.Selected == previousSelect)
            {
                changeAnswerBackground(previousSelect, "leftNormalAnswer", "RightNormalAnswer");
                previousSelect = ' ';
            }
            else
            {
                changeAnswerBackground(QuestionReader.Selected, "leftSelectionAnswer", "rightSelectionAnswer");
                changeAnswerBackground(previousSelect, "leftNormalAnswer", "RightNormalAnswer");
                previousSelect = QuestionReader.Selected;
            }


        }
        private void changeAnswerBackground(char c,string leftBackgrounds,string rightBackgrounds)
        {
            Image imSelect = null;
            switch (c)
            {
                case 'A':
                    imSelect = new Bitmap(@"graphics\"+leftBackgrounds+".png");
                    LeftTopAnswerLayout.BackgroundImage = imSelect;
                    break;
                case 'C':
                    imSelect = new Bitmap(@"graphics\" + leftBackgrounds + ".png");
                    LeftBottomAnswerLayout.BackgroundImage = imSelect;
                    break;
                case 'B':
                    imSelect = new Bitmap(@"graphics\" + rightBackgrounds + ".png");
                    RightTopAnswerLayout.BackgroundImage = imSelect;
                    break;
                case 'D':
                    imSelect = new Bitmap(@"graphics\" + rightBackgrounds + ".png");
                    RightBottomAnswerLayout.BackgroundImage = imSelect;
                    break;
                default:
                    break;
            }
        }
        private void help(Form f1, EventArgs e)
        {
            switch (HelpUtil.Instance.Number)
            {
                case 1:
                    setHelpBackground(HelpUtil.Instance.helpList[0].WasUsed, help50, "50");
                    Question quest = questionReader.getQuestion();
                    int counter = 0;
                    foreach (Answer ans in quest.Answers)
                    {
                        if (!ans.isTrue&&counter<2)
                        {
                            helper(ans.letter);
                            counter++;
                        }
                    }
                        break;
                case 2:
                    setHelpBackground(HelpUtil.Instance.helpList[1].WasUsed, helpPublic, "Phone");
                    break;
                case 3:
                    setHelpBackground(HelpUtil.Instance.helpList[2].WasUsed, helpPhone, "Public");
                    break;
                default:
                    break;
            }
        }
        private void helper(char letter)
        {
            switch (letter)
            {
                case 'A':
                    AAnswer.Text = "";
                    break;
                case 'C':
                    CAnswer.Text = "";
                    break;
                case 'B':
                    BAnswer.Text = "";
                    break;
                case 'D':
                    DAnswer.Text = "";
                    break;
                default:
                    break;
            }
        }
        private void setHelpBackground(bool wasUsed, TableLayoutPanel tlp,string image)
        {
            Image bgl = null;

            if (!wasUsed)
            {
               bgl = new Bitmap(@"graphics\underline" + image + ".png");
            }
            else
            {
                bgl = new Bitmap(@"graphics\used" + image + ".png");
            }
            tlp.BackgroundImage = bgl;
        }
        private void tableLayoutPanel14_Paint(object sender, PaintEventArgs e)
        {

        }
        private void restartAnswerBackgrounds()
        {
            Image bgl = new Bitmap(@"graphics\leftNormalAnswer.png");
            Image bgr = new Bitmap(@"graphics\RightNormalAnswer.png");
            LeftTopAnswerLayout.BackgroundImage = bgl;
            LeftBottomAnswerLayout.BackgroundImage = bgl;
            RightTopAnswerLayout.BackgroundImage = bgr;
            RightBottomAnswerLayout.BackgroundImage = bgr;
        }
        private void restartHelpBackgrounds()
        {
            Image bg1 = new Bitmap(@"graphics\50.png");
            Image bg2 = new Bitmap(@"graphics\public.png");
            Image bg3 = new Bitmap(@"graphics\phone.png");
            help50.BackgroundImage = bg1;
            helpPublic.BackgroundImage = bg2;
            helpPhone.BackgroundImage = bg3;
        }
    }
}
